# Створити адаптований аналог гри Whack a mole.

## Ideas

-  Win/lose modal window

-  Moles nicknames and personalities

## Bugs

### Bug 1: Loss of lives

**Description**: Sometimes upon clicking on mole, the hole loses 2 moles, instead of 1. Might be the problem with timer and player taking moles amount at the same time.

**Steps to Reproduce**:

1. Choose mole mode, difficulty as hard and 2+ moles
2. Start playing game
3. It happens randomly, so need to wait a bit

**Expected Result**: Every time a player clicks on cell, it loses 1 mole.

**Actual Result**: Upon clicking on cell, hole loses 2 moles randomly.

### Bug 2: Player don't lose lives

**Description**: Sometimes upon clicking on wrong cell, player wont lose lives. Everything works fine on timer mode and with easy difficulty, though needs more testing.

**Steps to Reproduce**:

1. Choose mole mode, difficulty as hard and 2+ moles
2. Start playing game
3. It happens randomly, so need to wait a bit

**Expected Result**: Whenever player misses correct cell - he/she should lose life.

**Actual Result**: Upon clicking on wrong cell, player dont lose life.

### Bug 3: Cell freeze

**Description**: Sometimes cells freeze and player can hit mole whenever he pleases, so seems like the mole timer doesn't start. Easier to notice on hard difficulty.

**Steps to Reproduce**:

1. Choose mole mode, difficulty as hard and 2+ moles
2. Start playing game
3. It happens randomly, so need to wait a bit

**Expected Result**: All cells must show mole and hide them after a period of time.

**Actual Result**: Mole doesn't hide randomly.
